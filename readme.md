* UnderWorldDB is a proyect created for the motivation to use sqlite in react-native and expo to use like mongo db to dont'touch sqlite


** this is an example to how to use in a simple file

	import UnderWorldDB from '@theevil24a/underworlddb'

	const TestSchema = () => {
	    return {
			db_name: "db.test",
	        table_name: "Test",
	        object: {
	            _id: {
	                not_null: true,
	                primay_key: true
	            },
	            username: {
	                foreign: {
	                    collection: 'User',
	                    key: '_id'
	                },
	                not_null: true
	            },
	            username: {
	                not_null: true
	            },
	            numero: {
	                not_null: true,
	                type: 'INTEGER'
	            },
	            modifyAt: {
	                default: new Date().getTime(),
	                not_null: true
	            },
	            synchronization: {
	                type: 'bit',
	                default: 0,
	                not_null: true
	            }
	        }
	    }
	}

	export const ControllerTestInset = () => {
	    let values = {
	        _id: 'asdasdasdasd',
	        idClient: 'asdasdasd',
	        hnombre: 'asdasdasd',
	        numero: 123123,
	        modifyAt: new Date().getTime(),
	        synchronization: 0
	    }

	    UnderWorldDB.insert( TestSchema(), values, ( error , response ) => {
	        console.log({error})
	        console.log('hi')
	        console.log(response)
	        if(!error){
	        }
	    });
	}

	export const ControllerTestFindAll = () => {
	    UnderWorldDB.findAll( TestSchema(), ( error , response ) => {
	        if(!error){
	            console.log('hi')
	            console.log(response)
	        }
	    });
	}

	export const ControllerTestUpdateSomting = () => {
	    let values = {
	        numero: 123123,
	        modifyAt: new Date().getTime(),
	        synchronization: 0
	    }
	    let id = 'asdasdasdasd'

	    UnderWorldDB.updateSomting( TestSchema(), values, id, ( error , response ) => {
	        if(!error){
	            console.log('hi')
	            console.log(response)
	        }
	    });
	}
