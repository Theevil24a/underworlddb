import * as SQLite from 'expo-sqlite';

export default new class UnderWorldDB {
insert = ( config, values, callback ) => {
        if (!'db_name' in config) return callback ('required-config: db_name', null)
        if (!'table_name' in config) return callback ('required-config: table-name', null)
        if (!'object' in config) return callback ('required-config: object', null)
        
        this.db = SQLite.openDatabase(config.db_name);
        let keys_string = ''
        for (let key in config.object){
            keys_string += `${key} ${'type' in config.object[key]?`${config.object[key].type.toUpperCase()}`:'TEXT'} `
            +`${'primay_key' in config.object[key]?'PRIMARY KEY':''} `
            +`${'autoincrement' in config.object[key]?'AUTOINCREMENT':''} `
            +`${'not_null' in config.object[key]?'NOT NULL':''} `
            +`${'default' in config.object[key]?`DEFAULT ${config.object[key].default} `:''} `
            +`${'foreign' in config.object[key]?`REFERENCES ${config.object[key].foreign.collection}(${config.object[key].foreign.key})`:''}`
            +`${Object.keys(config.object)[Object.keys(config.object).length - 1] == key?'':','} `
        }

        this.db.transaction(tx => {
            tx.executeSql(
                `CREATE TABLE IF NOT EXISTS ${config.table_name} (${keys_string});`, null,
                (txObj, resultSet) => {
                    let keys_insert = ''
                    let key_incgnit = ''
                    let list_data_array = []

                    for (let key in config.object){
                        keys_insert += `${key}${Object.keys(config.object)[Object.keys(config.object).length - 1] == key?'':','} `
                        key_incgnit += `?${Object.keys(config.object)[Object.keys(config.object).length - 1] == key?'':','} `

                        if ('not_null' in config.object[key] && !key in values){
                            return callback (`ERROR NOT_NULL KEY ${key}` , null)
                        }

                        if (!key in values){
                            if ('default' in config.object[key]){
                                list_data_array.push(config.object[key].default)

                            }else{
                                list_data_array.push('" "')
                            }
                        }else{
                            list_data_array.push(values[key])
                        }
                    }

                    this.db.transaction(tx => {
                        tx.executeSql(`INSERT INTO ${config.table_name} ( ${keys_insert} ) values ( ${key_incgnit} )`, list_data_array, (txObj, resultSet) => {
                            return callback ( null , resultSet)
                        }, (txObj, error) => {
                            return callback ( error , null)
                        })
                    });

                }, (txObj, error) => {
                    return callback ( error , null)
            })
        })
    }

    findAll ( config, callback ) {
        if (!'db_name' in config) return callback ('required-config: db_name', null)
        if (!'table_name' in config) return callback ( 'required-config: table-name' , null)        
        
        this.db = SQLite.openDatabase(config.db_name);
        this.db.transaction(tx => {
            tx.executeSql(`SELECT * FROM ${config.table_name}`, null, (txObj, { rows: { _array } }) => {
                return callback ( null , _array)
            }, (txObj, error) => {
                return callback ( error , null)
            })
        });
    }

    dropTable (list_tables, callback){
        if (!'db_name' in config) return callback ('required-config: db_name', null)
        this.db = SQLite.openDatabase(config.db_name);
        this.db.transaction(tx => {
            tx.executeSql( `DROP TABLE IF EXISTS ${list_tables};`,
                (txObj, resultSet) => {
                    return callback ( null , resultSet)
                },
                (txObj, error) => {
                    return callback ( error , null)
                }
            )
        })
    }


    updateSomting ( config, values, _id, callback ) {
        if (!'db_name' in config) return callback ('required-config: db_name', null)
        if (!'table_name' in config) return callback ( 'required-config: table-name' , null)
        this.db = SQLite.openDatabase(config.db_name);
        let keys_to_set = ''

        for (let key in values){
            keys_to_set += `${key} = ${values[key]}`
        }

        this.db.transaction(tx => {
			tx.executeSql(
                `UPDATE ${config.table_name} SET ${keys_to_set}  WHERE _id = ?`, [_id],
				(txObj, resultSet) => {
					if (resultSet.rowsAffected > 0) {
						return callback ( null , resultSet)
					}
                },
                (txObj, error) => {
                    return callback ( error , null)
                }
            )
		})
    }
}
